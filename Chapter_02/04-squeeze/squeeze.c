/* Exercise 2-4. Write an alternate version of squeeze(s1,s2) that deletes
 * each character in s1 that matches any character in the string s2.
 */

#include <stdio.h>

#define MAX_LENGHT 1000

void squeeze(char orig_string[], char char_to_remove[]);

int
main(int argc, char* argv[])
{
	char s1[MAX_LENGHT] = "This is some string to parse";
	char s2[MAX_LENGHT] = "i os";

	printf("The original string is:\n");
	printf("%s\n", s1);

	printf("Characters to be removed:\n");
	printf("%s\n", s2);

	squeeze(s1, s2);

	printf("Original string after squeeze:\n");
	printf("%s\n", s1);

	return 0;
}

void
squeeze(char s1[], char s2[])
{
	int i, j, k;
	int in_string = 0;

	for (i = j = 0; s1[i] != '\0'; i++) {
		for (k = 0; s2[k] != '\0'; k++) {
			/* TODO: check if the characters are equal, if they are, then skip that
			 * character from s1 and pass to the next character*/
			if (s1[i] == s2[k]) {
				in_string = 1;
			}
		}
		if (in_string == 1) in_string = 0;
		else s1[j++] = s1[i];
	}
	s1[j] = '\0';
}
