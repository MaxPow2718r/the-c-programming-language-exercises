/* Exercise 2-9. In a two's complement number system, x &= (x-1) deletes the
 * rightmost l-bit in x. Explain why. Use this observation to write a faster
 * version of bitcount.
 *
 * x = x & (x - 1) This subtract the least significant bit from x (thus the x -
 * 1). Hence x & (x - 1) would always delete the last bit, because it makes only
 * that bit different from the original number.
 */

#include <stdio.h>

int bitcount(int value);

int
main(int argc, char* argv[])
{
	int x, count;
	x = 0b10101011;

	printf("x = %d\n",  x);

	count = bitcount(x);

	printf("count = %d\n", count);
	return 0;
}

int
bitcount(int x)
{
	int b;

	for (b = 0; x != 0; x &= x - 1) {
		b++;
	}

	return b;
}
