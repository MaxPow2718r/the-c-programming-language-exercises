/* Exercise 2.3. Write the function htoi (s), which converts a string of hexa-
 * decimal digits (including an optional 0x or 0X) into its equivalent integer
 * value.  The allowable digits are 0 through 9, a through f, and A through F.
 */

#include <stdio.h>
#include <math.h>

/* Maximum value for an unsigned integer would be FFFF (65535). The maximum
 * posible value would always be 16^(MAX_NUMBER) - 1 */
#define MAX_NUMBER 4

/* enum hex_val { A = 10, B, C, D, E, F }; */

unsigned int htoi(char hex_string[], int len);
int upper(int character);
int get_line(char buffer_string[]);

int
main(int argc, char* argv[])
{
	int integer, len;
	char hex[MAX_NUMBER + 1];

	len = get_line(hex);

	if (len == 0) {
		printf("The maximun supported length is %d\n", MAX_NUMBER);
		return 0;
	}

	integer = htoi(hex, len);

	printf("%d\n", integer);

	return 0;
}

unsigned int
htoi(char s[], int len)
{
	unsigned int value = 0;
	int i = len - 1;

	while (i >= 0) {
		if (s[i] >= 'A' && s[i] <= 'F') {
			value = (s[i] - 55) * pow(16, len - i - 1) + value;
		}
		else {
			value = (s[i] - '0') * pow(16, len - i - 1) + value;
		}
		--i;
	}

	return value;
}

int
upper(int c)
{
	if (c >= 'a' && c <= 'z') {
		return c + 'A' - 'a';
	}
	else {
		return c;
	}
}

int
get_line(char s[])
{
	int c, i;

	for (i = 0; (c = getchar()) != EOF; ++i) {
		if (i > MAX_NUMBER) {
			printf("Not valid lenght\n");
			return 0;
		}

		c = upper(c);
		s[i] = c;
	}

	return i - 1;
}
