/* Exercise 2-6. Write a function setbits(x,p,n,y) that returns x with the n
 * bits that begin at position p set to the rightmost n bits of y, leaving the
 * other bits unchanged. 0
 */

#include <stdio.h>

int setbits(int x, int position, int n, int y);

int
main(int argc, char* argv[])
{
	int x, p, n, y;

	/* 0x97 = 0b10010111 = 151_dec*/
	x = 0x97;
	p = 3;
	n = 2;
	y = 0x12;
	/* los n = 2 bits de x en la posicion p = 3 (partiendo de 0) son 01, y es
	 * 0x12 = 0b00010010 = 18_dec, asi que se debería escribir 01 en y.
	 * Finalmente se deberia retornarse x como 0b00010001 (17_dec)
	 */
	x = setbits(x, p, n, y);

	printf("x = %d\n", x);
	return 0;
}

int
setbits(int x, int p, int n, int y)
{
	int aux;

	/* moves the targe bits to the righmost position */
	aux = x >> p - 1;
	/* sets to zero every bit but the last n */
	aux = aux & ~(~0 << n);
	/* the las n bits of y  to zero */
	y = y & (~0 << n);

	return y | aux;
}
