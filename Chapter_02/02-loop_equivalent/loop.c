/* Exercise 2-2. Write a loop equivalent to the for loop above (commented out)
 * without using &&  or ||
 */

#include <stdio.h>

int
main(int argc, char* argv[])
{
	int c;
	int lim = 15;

	/* for (int i = 0; i < lim - 1 && (c = getchar()) != '\n' && c != EOF; i++) { */
	/* 	putchar(c); */
	/* } */

	for (int i = 0; i < lim - 1; i++) {
		if ((c = getchar()) != '\n') {
			if (c != EOF) putchar(c);
		}
	}

	return 0;
}
