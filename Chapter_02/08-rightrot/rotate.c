/* Exercise 2-8. Write a function rightrot(x, n) that returns the value of the
 * integer x rotated to the right by n bit positions.
 */

#include <stdio.h>

unsigned int rot(unsigned int x, int n);

int
main(int argc, char* argv[])
{
	unsigned int x;
	int n;

	/* 0x92 = 0b10010010 = 146_dec */
	x = 0x92;
	n = 6;

	printf("x = %d\n", x);
	x = rot(x, n);

	printf("x = %d\n", x);

	return 0;
}

unsigned int
rot(unsigned int x, int n)
{
	/* This works but only for 8 bit long numbers */
	/* x = ~(~0 << 8) & ((x >> n) | (x << 8 - n)); */
	x = 0xFFFFFFFF & ((x >> n) | (x << 32 - n));

	return x;
}
