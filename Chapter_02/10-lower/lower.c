/* Exercise 2-10. Rewrite the function lower, which converts upper case letters
 * to lower case, with a conditional expressioninstead of if-else.
 */

#include <stdio.h>

#define MAX_LENGHT 1000

void lower(char upper_case_string[]);

int
main(int argc, char* argv[])
{
	char s[MAX_LENGHT] =
		"Exercise 2-10. Rewrite the function lower, which "
		"converts upper case letters to lower case, with a conditional "
		"expressioninstead of if-else.";

	printf("%s\n", s);
	lower(s);

	printf("%s\n", s);
	return 0;
}

void
lower(char s[])
{
	int i;
	for (i = 0; s[i] != '\0'; i++) {
		s[i] = (s[i] > 'A' && s[i] < 'Z') ? s[i] + 'a' - 'A' : s[i];
	}
}
