/* Exercise 2-5. Write the function any(s1, s2), which returns the first
 * location in the string s1 where any character from the string s2 occurs, or
 * -1 if s1 contains no characters from s2. (The standard library function
 *  strpbrk does the same job but returns a pointer to the location.)
 */

#include <stdio.h>

#define MAX_LENGHT 1000

int any(char orig_string[], char to_find[]);

int
main(int argc, char* argv[])
{
	int pos;
	char s1[MAX_LENGHT] = "This is some string to parse";
	char s2[MAX_LENGHT] = "i os";

	printf("The original string is:\n");
	printf("%s\n", s1);

	printf("Characters to be find:\n");
	printf("%s\n", s2);

	pos = any(s1, s2);

	printf("pos %d", pos);

	return 0;
}

int
any(char s1[], char s2[])
{
	int i, j;

	for (i = 0; s1[i] != '\0'; i++) {
		for (j = 0; s2[j] != '\0'; j++) {
			if (s1[i] == s2[j])
				return i;
		}
	}

	return -1;
}
