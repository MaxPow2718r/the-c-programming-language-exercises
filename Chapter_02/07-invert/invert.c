/* Exercise 2-7. Write a function invert(x, p, n) that returns x with the n bits
 * that begin at position p inverted (i.e., I changed into 0 and vice versa),
 * leaving the others unchanged.
 */

#include <stdio.h>

int invert(int x, int position, int n);

int
main(int argc, char* argv[])
{
	int x, p, n;

	/* 0x64b5 = 0b0110010010110101 = 25781_dec invert 6 bits starting from
	 * position 5 (counting from zero from the right) are 010010 inverted are
	 * 101101 x should be at the end: 0b0110101101110101 = 0x6b65 = 27509_dec
	 */
	x = 0x64b5;
	p = 5;
	n = 6;

	printf("x = %d\n", x);
	x = invert(x, p, n);

	printf("x = %d\n", x);

	return 0;
}

int
invert(int x, int p, int n)
{
	int aux;

	aux = (x >> p + 1) & ~(~0 << n);
	aux = ~(aux | (~0 << n)) << p + 1;
	x = (x & (~0 << p + n)) | (x & ~(~0 << p + 1));

	return x | aux;
}
