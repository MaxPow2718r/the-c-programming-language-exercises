/* Exercise 2-1. Write a program to determine the ranges of char, short, int,
 * and long variables, both signed and unsigned, by printing appropriate values
 * from standard headers and by direct computation.  Harder if you compute them
 * determine the ranges of various floating-point types.
 */

#include <stdio.h>
#include <limits.h>
#include <math.h>

#define UMIN 0

int
main(int argc, char* argv[])
{
	printf("The limits of signed variables are:\n");
	printf("The limits of char are: %d %d\n", SCHAR_MIN, SCHAR_MAX);
	printf("The limits of short are: %d %d\n", SHRT_MIN, SHRT_MAX);
	printf("The limits of int are: %d %d\n", INT_MIN, INT_MAX);
	printf("The limits of long are: %ld %ld\n", LONG_MIN, LONG_MAX);

	printf("\n");
	printf("The limits of unsigned variables are:\n");
	printf("The limits of char are: %d %d\n", UMIN, UCHAR_MAX);
	printf("The limits of short are: %d %d\n", UMIN, USHRT_MAX);
	printf("The limits of int are: %ld %ld\n", UMIN, UINT_MAX);
	printf("The limits of long are: %lu %lu\n", UMIN, ULONG_MAX);

	printf("\n");

	int max_char = pow(2, 8);
	printf("The limits of char are: %d %d", 0, max_char);
	printf("%lu", pow(2, 64));

	return 0;
}
