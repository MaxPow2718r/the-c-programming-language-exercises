/* Exercise 3-1. Our binary search makes two tests inside the loop, when one
 * would suffice (at the price of more tests outside). Write a version with only
 * one test inside the loop and measure the difference in run-time.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LENGTH 1000000

int binsearch_orig(int value, double array[], int array_length);
int binsearch_new(int value, double array[], int array_length);
void gen_array(int array_length, double array[]);

int
main(int argc, char* argv[])
{
	srand(time(NULL));

	clock_t t;
	double time_orig, time_new;

	int x, n;
	int result_orig, result_new;

	double v[MAX_LENGTH];
	gen_array(MAX_LENGTH, v);

	x = rand() % MAX_LENGTH;
	n = MAX_LENGTH;

	t = clock();
	result_orig = binsearch_orig(x, v, n);
	t = clock() - t;

	time_orig = ((double)t) / CLOCKS_PER_SEC;

	t = clock();
	result_new = binsearch_new(x, v, n);
	t = clock() - t;

	time_new = ((double)t) / CLOCKS_PER_SEC;

	printf("The number to be search is: %d\n", x);
	printf("It took the original method %lf seconds to execute\n", time_orig);
	printf("It took the new method %lf seconds to execute\n", time_new);

	printf("The original method returned: %d\n", result_orig);
	printf("The new method returned: %d\n", result_new);

	return 0;
}

int
binsearch_new(int x, double v[], int n)
{
	int low, high, mid;

	low = 0;
	high = n - 1;
	while (low < high) {
		mid = (low + high) / 2;
		if (v[mid] >= x)
			high = mid;
		else
			low = mid + 1;
	}

	return v[high] == x ? high : -1;
}

int
binsearch_orig(int x, double v[], int n)
{
	int low, high, mid;

	low = 0;
	high = n - 1;
	while (low <= high) {
		mid = (low + high) / 2;
		if (x < v[mid])
			high = mid - 1;
		else if (x > v[mid])
			low = mid + 1;
		else
			return mid;
	}
	return -1;

}

/* Generates an array with fixed length witch numbers are incremental but
 * randomly generated
 */
void
gen_array(int length, double array[])
{
  for (int i = 0; i < length; i++)
    array[i] = array[i - 1] + rand() % 10;
}
