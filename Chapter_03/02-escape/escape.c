/* Exercise 3-2. Write a function escape(s, t) that converts characters like
 * newline and tab into visible escape sequences like \n and \t as it copies
 * the string t to s. Use a switch. Write a function for the other direction as
 * well, converting escape sequences into the real characters.
 */

#include <stdio.h>

#define MAX_LENGTH 100000

void escape(char original_string[], char end_string[]);
void reverse_escape(char original_string[], char end_string[]);
void get_string(char string[]);

int
main(int argc, char* argv[])
{
	char s[MAX_LENGTH];
	char t[MAX_LENGTH];
	char u[MAX_LENGTH];

	get_string(s);
	escape(s, t);
	reverse_escape(t, u);

	printf("%s", t);
	printf("\n\n");
	printf("####################");
	printf("\n\n");
	printf("%s", u);

	return 0;
}

void
reverse_escape(char t[], char s[])
{
	int i, j;

	for (i = j = 0; t[i] != '\0'; i++) {
		switch (t[i]) {
			case 't':
				/* (s[j - 1] == '\\') ? s[--j] = '\t' : s[j++] = t[i]; */
				if (s[j - 1] == '\\') {
					s[j - 1] = '\t';
				}
				else
					s[j++] = t[i];
				break;
			case 'b':
				if (s[j - 1] == '\\') {
					s[j - 1] = '\b';
				}
				else
					s[j++] = t[i];
				break;
			case 'n':
				if (s[j - 1] == '\\') {
					s[j - 1] = '\n';
				}
				else
					s[j++] = t[i];
				break;
			default:
				s[j++] = t[i];
				break;
		}
	}
}

void
escape(char s[], char t[])
{
	int i, j;

	for (i = j = 0; s[i] != '\0'; i++) {
		switch (s[i]) {
			case '\t':
				t[j] = '\\';
				t[++j] = 't';
				break;
			case '\b':
				t[j] = '\\';
				t[++j] = 'b';
				break;
			case '\n':
				t[j] = '\\';
				t[++j] = 'n';
				break;
			default:
				t[j] = s[i];
				break;
		}
		j++;
	}
}

void
get_string(char s[])
{
	int c;
	int i = 0;

	while ((c = getchar()) != EOF) {
		s[i++] = c;
	}
	s[i] = '\0';
}
