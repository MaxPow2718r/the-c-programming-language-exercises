/* Exercise 3-6. Write a version of itoa that accepts three arguments instead of
 * two. The third argument is a minimum field width; the converted number must
 * be padded with blanks on the left if necessary to make it wide enough.
 */

#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 10000

void itoa(int number, char string[], int padding);
void reverse(char string[]);

int
main(int argc, char* argv[])
{
	int n = 12345678;
	int padding = 10;
	char s[MAX_LENGTH];
	itoa(n, s, padding);

	printf("%d\n", n, padding);

	printf("\n");
	printf("%s\n", s);
	return 0;
}

void
itoa(int number, char string[], int padding)
{
	int i, sign;

	if ((sign = number) < 0)
		number = -number;

	i = 0;
	do {
		string[i++] = number % 10 + '0';
	} while ((number /= 10) > 0);

	if (i < padding) {
		do {
			string[i++] = ' ';
		} while (i < padding);
	}

	if (sign < 0)
		string[i++] = '-';

	string[i] = '\0';

	reverse(string);
}

void
reverse(char s[])
{
	int i, j, c;

	for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
}
