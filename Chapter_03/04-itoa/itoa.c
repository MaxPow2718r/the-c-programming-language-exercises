/* Exercise 3-4. In a two's complement number representation, our version of
 * itoa does not handle the largest negative number, that is, the value of n
 * equal to -(2^(wordsize-l)). Explain why not. Modify it to print that value
 * correctly, regardless of the machine on which it runs.
 *
 * I don't understand this question nor what is been requested
 */

#include <stdio.h>

#define MAX_LENGTH 10000

void itoa(int number, char string[]);

int
main(int argc, char* argv[])
{
	int n = 12345678;
	char s[MAX_LENGTH];
	itoa(n, s);

	printf("%d\n", n);

	printf("\n");
	printf("%s\n", s);
	return 0;
}

void
itoa(int number, char string[])
{
	int i = 0;

	do {
		string[i++] = number % 10 + '0';
	} while ((number /= 10) > 0);
}
