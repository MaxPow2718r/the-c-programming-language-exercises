/* Exercise 3-3. Write a function expand (s1, s2) that expands shorthand
 * notations like a-z in the string s1 into the equivalent complete list
 * abc...xyz in s2. Allow for letters of either case and digits, and be prepared
 * to handle cases like a-b-c and a-zO-9 and -a-z.  Arrange that a leading or
 * trailing - is taken literally.
 */

#include <stdio.h>

#define MAX_LENGTH 10000

void expand(char short_hand_expresion[], char expanded_notation[]);

int
main(int argc, char* argv[])
{
	char s1[MAX_LENGTH] = "Some text blah, z-a blah a-z a-d, -A-Z0-9-, etc, etc";
	char s2[MAX_LENGTH];

	printf("%s\n", s1);
	expand(s1, s2);

	printf("%s\n", s2);

	return 0;
}

void
expand(char from[], char to[])
{
	int i, j;
	int distance, diff;

	diff = 1;
	for (i = j = 0; from[i] != '\0'; i++) {
		if (from[i] == '-') {
			if (from[i - 1] != ' ' && from[i + 1] != ' ') {
				distance = from[i + 1] - from[i - 1];

				if (distance > 0) {
					while (diff < distance) {
						to[j++] = from[i - 1] + diff++;
					}

					diff = 1;
				}

				else to[j++] = from[i];
			}

			else to[j++] = from[i];

		}
		else to[j++] = from[i];
	}
}
