/* Exercise 3-5. Write the function itob(n, s, b) that converts the integer n
 * into a base b character representation in the string s. In particular,
 * itob(n, s, 16) formats n as a hexadecimal integer in s.
 */

#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 1000

void itob(int number, char string[], int base);
void reverse(char string[]);

int
main(int argc, char* argv[])
{
	int n, b;
	n = -100;
	b = 2;
	char s[MAX_LENGTH];

	printf("Original number: %d\n", n);
	printf("Base: %d\n", b);
	itob(n, s, b);

	printf("Final number: %s\n", s);
	return 0;

}

void
itob(int n, char s[], int base)
{
	int i, pos_num;
	int padding;
	pos_num = (n >= 0) ? n : -n;

	i = 0;

	do {
		s[i++] = (((pos_num % base) / 10) < 1) ?
			(pos_num % base) + '0' :
			((pos_num % base) / 10) + 'A';
	} while((pos_num /= base) > 0);

	if (n < 0) s[i++] = '-';

	s[i] = '\0';

	reverse(s);
}

void reverse(char s[])
{
	int i, j, c;

	for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
}
