#include <stdio.h>

#define MAX_LENGHT 128

main()
{
	int c, i, j;
	int nchar[MAX_LENGHT];

	for (i = 0; i < MAX_LENGHT; ++i)
		nchar[i] = 0;

	while ((c = getchar()) != EOF) {
		++nchar[c];
	}

	for (i = 0; i < MAX_LENGHT; ++i)
		if (nchar[i] > 0) {
			if (i == ' ')
				printf("SPACE");
			else if (i == '\t')
				printf("\\t");
			else if (i == '\n')
				printf("\\n");
			else
				putchar(i);

			printf("\t");
			for (j = 0; j < nchar[i]; ++j) {
				printf("#");
			}
			printf("\n");
		}
}
