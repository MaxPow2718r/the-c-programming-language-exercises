#include <stdio.h>

#define MAXLINE 1000

int get_line(char line[], int lim);
void rmblank(char s[], int len);      

main()
{
	int c;
	int len;      
	char line[MAXLINE];
 
	while ((len = get_line(line, MAXLINE)) > 0) {  
		rmblank(line, len);
		printf("%s", line); 			
	}
}

int
get_line(char line[], int lim)
{ 			
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		line[i] = c;
	}
	
	if (c == '\n') {
		line[i] = c;
		++i;
	} 

	line[i] = '\0';
	return i;
}

void
rmblank(char s[], int len) 
{
	int i;

	if (len == 1 && s[0] == '\n') {
		s[0] = '\0';
	} 

	for (i = len - 2; i > 0; --i) {
		if (s[i] == ' ' || s[i] == '\t') {
			s[i] = '\0';
		} 
		else {
			i = 0;
		}
	} 
} 
