#include <stdio.h>

#define MAXLINE 1000

int get_line(char line[], int lim);
void reverse(char s[], int len);      

main()
{
	int c;
	int len;      
	char line[MAXLINE];
 
	while ((len = get_line(line, MAXLINE)) > 0) {  
		reverse(line, len);
		printf("%s", line); 			
	}
}

int
get_line(char line[], int lim)
{ 			
	int c, i;

	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		line[i] = c;
	}
	
	if (c == '\n') {
		line[i] = c;
		++i;
	} 

	line[i] = '\0';
	return i;
}

void
reverse(char s[], int len) 
{
	int i;
	char reversed[len];

	for (i = len - 2; i >= 0; --i) {
		reversed[len - (i + 2)] = s[i];
	} 

	reversed[len - 1] = s[len - 1];

	for (i = 0; i < len; ++i) {
		s[i] = reversed[i];
	} 
} 
