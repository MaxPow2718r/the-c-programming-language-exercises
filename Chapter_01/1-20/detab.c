#include <stdio.h>

/* Write a program 'detab' that replaces tabs in the input with the proper number
 * of blanks to space to the next tab stop. Assume a fixed set of tab stops, say
 * every n columns. Should n be a variable or a symbolic parameter?
 */

/* In this case n would be a symbolic parameter */
#define TABSTOP 4

void
main(void)
{
	int c;

	while ((c = getchar()) != EOF) {
		if (c == '\t') for (int i = 0; i < TABSTOP; i++) printf(" ");
		else putchar(c);
	}
}
