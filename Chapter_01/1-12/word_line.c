#include <stdio.h>

#define IN	1 							/* In blank */
#define OUT	0 							/* Out blank */

main()
{
	int c, state;

	state = IN; 						/* Last printed character was a blank */
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t' || c == '\n') {
			if (state == OUT) {
				c = '\n';
				state = IN;
				putchar(c);
			}
		}

		else {
			putchar(c);
			state = OUT;
		}

		/* if (state == IN) { */
		/* 	if (c != ' ' && c != '\t') { */
		/* 		c = '\b'; */
		/* 		state = OUT; */
		/* 	} */
		/* } */

	}
}
