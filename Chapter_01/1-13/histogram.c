#include <stdio.h>

#define IN	1			/* In a word */
#define OUT	0			/* Out of a word */

main()
{
	int i, c, lword, state;

	lword = 0;
	state = OUT;
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t' || c == '\n') {
			if (state == IN && lword > 0) {
				printf("\t\t\t\t");
				for (i = 0; i < lword; ++i) {
					printf("#");
				}
				printf("\n");
			}
			lword = 0;
			state = OUT;
		}
		else if (state == OUT) {
			state = IN;
			lword = 0;
		}

		if (state == IN)
			putchar(c);
			++lword;
	}
}
