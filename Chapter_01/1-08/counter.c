#include <stdio.h>

main()
{
	int tabs, blanks, new_lines;
	int c;

	new_lines = 0;
 	tabs = 0;
	blanks = 0;

	while ((c = getchar()) != EOF) {
		if (c == '\n') ++new_lines;
		if (c == '\t') ++tabs;
		if (c == ' ')  ++blanks;
	}
	printf("Thera are: %d new lines; %d tabs and %d blank spaces\n", new_lines, tabs, blanks);
}
