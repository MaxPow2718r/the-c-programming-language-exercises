#include <stdio.h>

main()
{
	int c, back_space, tab, back_slash, print_especial;

	while ((c = getchar()) != EOF) {
		print_especial = 0;
		if (c == '\t') {
			printf("\\t");
			print_especial = 1;
		}

		if (c == '\b') {
			printf("\\b");
			print_especial = 1;
		}

		if (c == '\\') {
			printf("\\\\");
			print_especial = 1;
		}

		if (print_especial == 0)
			putchar(c);
	}
}
