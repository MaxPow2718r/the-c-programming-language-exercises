#include <stdio.h>

float main ()
{
		float fahr, celsius;
		int lower, upper, step;

		lower    = -100;
		upper    = 300;
		step     = 20;
		celsius  = upper;
		printf("Celsius to Fahrenheit converter from %d to %d\n\n", lower, upper);
		printf("\tC\t\tF\n");

		while (celsius >= lower) {
				fahr = 9.0 * (celsius + 32.0) / 5.0;
				printf("\t%6.0f\t\t%8.2f\n", celsius, fahr);
				celsius = celsius - step;
		}
}
