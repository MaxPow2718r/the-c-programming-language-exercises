#include <stdio.h>

float fahr_to_celcius(float fahr);

main ()
{
	float fahr, celsius;
	int lower, upper, step;

	lower = 0;
	upper = 300;
	step  = 20;
	fahr  = lower;
	printf("Fahrenheit to Celsius converter from %d to %d\n\n", lower, upper);
	printf("\tF\t\tC\n");
	while (fahr < upper) {
		printf("\t%4.1f\t\t%6.2f\n", fahr, fahr_to_celcius(fahr));
		fahr = fahr + step;
	}
	return 0;
}

float
fahr_to_celcius(float fahr)
{
	float celsius;

	celsius = 5.0 * (fahr - 32.0) / 9.0;
	return celsius;
}
