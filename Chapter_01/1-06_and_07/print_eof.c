#include <stdio.h>

main()
{
	/* create three diferent integer */
	int a, b, c;
	c = EOF;

	/* prints the value of c, wich is EOF value */
	printf("EOF value is:\n");
	printf("%d\n", c);

	/* gives a value of 1 so a != c is true */
	a = 1;
	b = a != c;
	printf("getchar != EOF the values of:\n");
	printf("%d\n", b);

	/* gives a same value as c so is false */
	a = EOF;
	b = a != c;
	printf("%d\n", b);
}

